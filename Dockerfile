FROM tiangolo/uwsgi-nginx-flask:python3.10

ENV BASEDIR=/app
WORKDIR /app

copy ./requirements.txt ${BASEDIR}/requirements.txt
RUN pip install --no-cache-dir --upgrade pip
RUN pip install -r ${BASEDIR}/requirements.txt
copy . ${BASEDIR}

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

ENTRYPOINT ["python"]
CMD ["wsgi.py"]
