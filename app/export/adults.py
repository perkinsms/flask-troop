"""
export adults.py

given a list of adults, export a csv file with 
relevant information
"""

import csv
from app.models import Adult


def dump(adults=None, outfile="output.csv"):
    if not adults:
        adults = Adult.all()
    with open(outfile, "w", newline="") as f:
        writer = csv.DictWriter(f, ["name", "email"])
        writer.writeheader()

        for adult in adults:
            writer.writerow(
                {
                    "name": f'"{adult.lastname}, {adult.firstname}"',
                    "email": adult.email,
                }
            )
