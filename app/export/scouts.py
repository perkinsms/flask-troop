"""
export scouts.py

given a list of scouts, export a csv file with 
relevant information
"""

import csv
from app.models import Scout


def dump(scouts=None, outfile="output.csv"):
    if not scouts:
        scouts = Scout.all()
    with open(outfile, "w", newline="") as f:
        writer = csv.DictWriter(f, ["name", "email", "age", "patrol", "current_rank"])
        writer.writeheader()

        for scout in scouts:
            writer.writerow(
                {
                    "name": f'"{scout.lastname}, {scout.firstname}"',
                    "email": scout.email,
                    "age": scout.age,
                    "patrol": scout.patrol,
                    "current_rank": scout.current_rank_name,
                }
            )
