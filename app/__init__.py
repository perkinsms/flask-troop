"""
__init__.py

initialize flask app for troop manager software

"""
from flask import Flask
from flask_login import LoginManager
from app.config import Config
from app.extensions import (
    db,
    load_commands,
    load_blueprints,
)
from app.utils.logging import logger
from app.models import (
    User,
)

# from werkzeug.middleware.profiler import ProfilerMiddleware


def create_app(config_class=Config):
    """create the app, register config,
    add all the command line stuff,
    register the blueprints,
    return the app
    """
    logger.info("started the app")
    app = Flask(__name__)
    #    app.wsgi_app = ProfilerMiddleware(
    #        app.wsgi_app, sort_by=("tottime", "calls", "time"), restrictions=(40,)
    #    )

    app.config.from_object(config_class)

    load_commands(app)

    # Flask Extensions Here
    db.init_app(app)
    logger.info("loaded the database")

    login_manager = LoginManager()
    login_manager.login_view = "users.login"
    login_manager.init_app(app)

    @login_manager.user_loader
    def load_user(user_id):
        return db.session.query(User).filter(User.id == user_id).one_or_none()

    load_blueprints(app)

    return app


app = create_app()
