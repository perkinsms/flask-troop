"""
load_groups.py

loads patrols and other groups from file

note: this only creates the groups
load_scouts inserts the scout into the groups

depends:
none

usage:
import load_groups
load_groups.main()

"""
import csv

from tqdm import tqdm

from app.extensions import db
from app.models import Group
from app.utils.logging import log_it
from app.config import Config

groups_file = Config.FILES["groups"]


@log_it
def add_group(name):
    group = Group(name=name)
    db.session.add(group)
    return group


def import_groups(file):
    with open(file, encoding="utf-8-sig") as f:
        reader = csv.reader(f)
        lines = len(f.readlines())
        f.seek(0)
        line = reader.__next__()
        for line in tqdm(reader, total=lines, desc="Groups"):
            print(f"adding group {line[0]}")
            if line[0] is None:
                line[0] = "No Patrol"

            if not Group.get_group(line[0]):
                add_group(line[0])
        db.session.commit()


def main():
    import_groups(groups_file)


if __name__ == "__main__":
    main()
