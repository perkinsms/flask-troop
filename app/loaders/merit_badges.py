import csv

from tqdm import tqdm

from datetime import datetime
from app.extensions import db
from app.config import Config
from app.models import (
    Scout,
    MBSignoff,
    MBRequirement,
    MeritBadge,
)

from app.utils import parse_name, parse_merit_badge
from app.utils.logging import logger, log_it


mb_file = Config.FILES["merit_badges"]
mb_names = Config.FILES["merit_badge_names"]


@log_it
def create_badge(mb_name):
    b = db.session.query(MeritBadge).filter(MeritBadge.name == mb_name).one_or_none()
    if not b:
        b = MeritBadge(name=mb_name)
        db.session.add(b)
    mbr = get_mb_requirement(mb_name)
    return b, mbr


def get_mb(mb_name, version=None, eagle_required=False):
    b = db.session.query(MeritBadge).filter(MeritBadge.name == mb_name).one_or_none()
    if not b:
        b = MeritBadge(
            name=mb_name,
            eagle_required=eagle_required,
            version=version,
        )
        db.session.add(b)
        db.session.commit()
    else:
        b.eagle_required = eagle_required
        b.version = version
    return b


def get_scout(line):
    name = parse_name(line["Scout"])
    s = Scout.get(
        lastname=name.get("lastname", ""), firstname=name.get("firstname", "")
    )
    if not s:
        raise ValueError(f"Scout Not Found: {line['Scout']}")
    return s


def get_mb_requirement(mb_name):
    mbr = (
        db.session.query(MBRequirement)
        .filter(MBRequirement.badge_name == mb_name)
        .one_or_none()
    )
    if not mbr:
        mbr = MBRequirement(
            badge=get_mb(mb_name),
            code=f"{mb_name}-Completion",
            shortcode="Completion",
        )
        db.session.add(mbr)
        db.session.commit()
    return mbr


def get_mb_signoff(scout, requirement, date=None):
    mb_signoff = (
        db.session.query(MBSignoff)
        .filter(MBSignoff.scout_id == scout.id)
        .filter(MBSignoff.code == requirement.code)
        .one_or_none()
    )
    if not mb_signoff:
        mb_signoff = MBSignoff(scout=scout, requirement=requirement, date=date)
        db.session.add(mb_signoff)
        db.session.commit()
    else:
        mb_signoff.date = date


def import_merit_badges(file):
    with open(file, encoding="utf-8-sig") as f:
        reader = csv.DictReader(f)
        lines = len(f.readlines())
        f.seek(0)
        for line in tqdm(reader, total=lines, desc="Merit Badges"):
            mb_name, version, eagle_required = parse_merit_badge(
                line["Merit Badge"]
            ).values()
            logger.debug(
                f"in import merit badges: {mb_name}, {version}, {eagle_required}"
            )
            date = datetime.strptime(line["Earned"], "%m/%d/%y").date()
            b = get_mb(mb_name, version, eagle_required)
            s = get_scout(line)
            mbr = get_mb_requirement(mb_name)
            signoff = get_mb_signoff(scout=s, requirement=mbr, date=date)
    db.session.commit()


def make_badges_from_file(file):
    with open(file, newline="\n") as f:
        lines = len(f.readlines())
        f.seek(0)
        for line in tqdm(f, total=lines, desc="Merit Badges"):
            if not line:
                continue
            create_badge(line.strip())


def main():
    make_badges_from_file(mb_names)
    import_merit_badges(mb_file)
    db.session.commit()


if __name__ == "__main__":
    main()
