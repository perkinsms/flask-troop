"""
load_scouts.py

loads scouts from text file

usage:
import load_scouts
load_scouts.main()
"""
import csv

from app.extensions import db
from app.config import Config

from app.models import Scout, Rank, User
from app.utils.logging import log_it

scouts_file = Config.FILES["scouts"]
scouts_file = (
    "/home/perkinsms/Downloads/RosterReport_BSA Troop 162 All Scouts_20230329.csv"
)


def create_username(firstname, lastname):
    maybe_username = f"{lastname}{firstname}"
    for i in range(5, len(maybe_username)):
        username = (
            db.session.query(User)
            .filter(User.username == maybe_username[:i])
            .one_or_none()
        )
        if not username:
            return maybe_username[:i]


@log_it
def add_scout(line):
    scout = Scout(name=f"{line.get('Last Name')}, {line.get('First Name')}")
    db.session.add(scout)
    return scout


def add_more_information(scout, line):
    scout.patrol = line.get("Patrol")

    scout.email = line.get("Email1") or line.get("Email2") or line.get("Email3", None)
    scout.username = create_username(line.get("First Name"), line.get("Last Name"))
    scout.age = line.get("Age")

    # default rank is no rank
    scout.current_rank = Rank.named(line.get("Rank")) or Rank.named("No Rank")


def add_scout_to_patrol(scout, patrol):
    patrol.members.append(scout)


def import_scouts(file):
    with open(file, encoding="utf-8-sig", newline="") as f:
        reader = csv.reader(f)
        for line in reader:
            if "YOUTH MEMBERS" in line:
                break
        reader = csv.DictReader(f)
        for line in reader:
            scout = add_scout(line)
            add_more_information(scout, line)

            print(scout)


def main():
    import_scouts(scouts_file)


if __name__ == "__main__":
    main()
