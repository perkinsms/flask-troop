"""
adults.py

loads adults file

depends:
none

usage:
import load_adults
load_adults.main()

"""
import csv
from tqdm import tqdm

from app.extensions import db
from app.models import Adult, User
from app.utils.logging import logger, log_it
from app.utils import parse_name
from app.config import Config

adults_file = Config.FILES["adults"]


def create_username(text):
    lastname, firstname, *_ = parse_name(text).values()
    maybe_username = f"{lastname}{firstname}"
    for i in range(5, len(maybe_username)):
        username = (
            db.session.query(User)
            .filter(User.username == maybe_username[:i])
            .one_or_none()
        )
        if not username:
            return maybe_username[:i]


@log_it
def add_adult(line):
    name = parse_name(line.get("Name"))
    adult = Adult(
        lastname=name.get("lastname", ""), firstname=name.get("firstname", "")
    )
    adult.email = line.get("Email") or line.get("Email #2")
    adult.username = create_username(line.get("Name"))
    adult.phone = (
        line.get("Cell Phone")
        or line.get("Home Phone")
        or line.get("Business Phone")
        or None
    )

    adult.rank_signer = True if "Scoutmaster" in line.get("Leadership") else False
    db.session.add(adult)
    return adult


def import_adults(file):
    with open(file, encoding="utf-8-sig") as f:
        lines = len(f.readlines()) - 1
        reader = csv.DictReader(f)
        f.seek(0)

        for line in tqdm(reader, total=lines, desc="Adults"):
            logger.info(f"Adding Adult {line.get('Name')}")
            if not Adult.get(line.get("Name")):
                a = add_adult(line)
        db.session.commit()


def t_or_f(text: str):
    return True if text == "True" else False


def main():
    import_adults(adults_file)


if __name__ == "__main__":
    main()
