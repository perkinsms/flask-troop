"""load_requirements.py

used to load the requirements from file

depends on load_ranks

usage:
import load_requirements
load_requirements.main()

"""
import toml
from app.extensions import db
from app.models import (
    RankRequirement,
    Rank,
)
from app.config import Config
from app.utils.logging import log_it

REQUIREMENTS_FILE = Config.FILES["requirements"]


@log_it
def add_requirement(rank_obj, code, requirement_data):
    r = RankRequirement(
        badge=rank_obj,
        code=f"{rank_obj.name}-{code}",
        shortcode=code,
        text=requirement_data["text"],
        full_text=requirement_data["full_text"],
    )
    db.session.add(r)
    return r


def import_requirements(file):
    data = toml.load(file)
    for rank in data:
        print(f"adding rank: {rank}")
        for code, requirement_data in data[rank]["requirements"].items():
            rank_obj = Rank.named(rank)
            if not RankRequirement.get(code=f"{rank}-{code}"):
                add_requirement(rank_obj, code, requirement_data)

    db.session.commit()


def main():
    import_requirements(REQUIREMENTS_FILE)


if __name__ == "__main__":
    main()
