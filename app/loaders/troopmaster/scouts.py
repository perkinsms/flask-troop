"""
load_scouts.py

loads scouts from text file

depends:
load_ranks
load_groups

usage:
import load_scouts
load_scouts.main()
"""
from tqdm import tqdm

from app.config import Config

from app.extensions import db
from app.models import Scout, Group, Rank, User
from app.utils import parse_name
from app.utils.logging import logger, log_it

from tm_parser.tm_parser import Parser

scouts_file = Config.FILES["scouts"]

SIGNING_POSITIONS = [
    "Troop Guide",
    "Patrol Leader",
    "ASPL",
    "Assistant SPL",
    "Senior Patrol Leader",
    "Instructor",
]


NON_SIGNING_POSITIONS = ["Asst Patrol Leader"]


def can_sign(positions):
    """returns true if the scouts' positions include one from the SIGNING_POSITIONS
    and isn't one of the NON_SIGNING_POSITIONS (e.g. "Asst Patrol Leader")
    and the position is current (i.e., hasn't ended yet)"""
    if not positions:
        return False
    for item in positions:
        if (
            item.get("title") in SIGNING_POSITIONS
            and item.get("title") not in NON_SIGNING_POSITIONS
            and "end" not in item
        ):
            return True
    return False


def create_username(text):
    lastname, firstname, *_ = parse_name(text).values()
    maybe_username = f"{lastname}{firstname}"
    for i in range(5, len(maybe_username)):
        username = (
            db.session.query(User)
            .filter(User.username == maybe_username[:i])
            .one_or_none()
        )
        if not username:
            return maybe_username[:i]


@log_it
def add_scout(text):
    name = parse_name(text)
    scout = Scout(lastname=name["lastname"], firstname=name["firstname"])
    db.session.add(scout)
    return scout


def add_more_information(scout, data):
    scout.patrol = data.get("Patrol", None)
    scout.email = data.get("Email", None)
    scout.username = create_username(data.get("Name"))
    scout.age = data.get("Age", None)
    # default rank is no rank
    scout.current_rank = Rank.named(data.get("Rank")) or Rank.named("No Rank")


def add_signing(scout, leadership):
    scout.rank_signer = can_sign(leadership) and scout.current_rank > Rank.named(
        "Second Class"
    )


def add_scout_to_patrol(scout, patrol):
    if scout not in patrol.members:
        patrol.members.append(scout)


def import_scouts(file):
    p = Parser(infile=file, outfile=None, file_format="json")
    length = len(p)
    for name, scout_data in tqdm(p, total=length, desc="Scouts"):
        scoutname = parse_name(name)
        logger.debug(f"importing scout {name}")
        if "Data" in scout_data:
            data = scout_data.get("Data")
        else:
            continue
        group = Group.get_group(data.get("Patrol"))
        logger.debug(f"importing scout into group: {group.name}")
        logger.debug(f"in import_scouts: {name}, {data}")
        scout = Scout.named(
            lastname=scoutname.get("lastname", ""),
            firstname=scoutname.get("firstname", ""),
        )
        if not scout:
            scout = add_scout(data.get("Name"))
        add_more_information(scout, data)
        add_scout_to_patrol(scout, group)
        add_signing(scout, scout_data.get("Leadership"))
        db.session.commit()


def main():
    import_scouts(Config.FILES["tm_data"])


if __name__ == "__main__":
    main()
