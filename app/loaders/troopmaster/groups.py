"""
load_groups.py

loads patrols and other groups from file

note: this only creates the groups
load_scouts inserts the scout into the groups

depends:
none

usage:
import load_groups
load_groups.main()

"""
from app.extensions import db
from app.models import Group
from app.utils.logging import log_it
from app.config import Config

from tm_parser.tm_parser import Parser

groups_file = Config.FILES["tm_data"]


@log_it
def add_group(name):
    group = Group.named(name)
    if not group:
        group = Group(name=name)
        db.session.add(group)
        db.session.commit()
    return group


def import_groups(file):
    p = Parser(infile=file, outfile=None, file_format="toml")
    for name, scout_data in p:
        if "Patrol" not in scout_data["Data"]:
            add_group("No Patrol")
            continue
        if not Group.get_group(scout_data["Data"].get("Patrol")):
            print(f"adding group {scout_data['Data'].get('Patrol')}")
            add_group(scout_data["Data"].get("Patrol"))
        db.session.commit()


def main():
    import_groups(groups_file)


if __name__ == "__main__":
    main()
