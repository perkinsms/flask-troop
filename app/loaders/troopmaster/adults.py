import re
from pprint import pprint

# pymupdf
import fitz
from more_itertools import split_before


file = "data/162/AdultPersonalData.PDF"


class Config:
    WHOLE_PAGE = [0, 0, 612, 792]


def get_adults(file):
    """take a file handle that's been opened
    strip out all the bad lines
    and return lists of lines - each list is one scout's information
    """
    return separate_adults(get_good_lines(file))


def separate_adults(lines):
    """take the collection of all lines and divide them into groups of information
    pertaining to an individual scout"""
    return split_before(lines, lambda x: x.startswith("Name:"))


def good_line(line):
    """a filter that rejects lines that should not be
    in the final report

    no blank lines
    no lines starting with "Page"
    no lines starting with "Verified
    no lines with "Individual History"
    no lines with "Position not credited toward rank"
    no lines with "(continued)"
    no lines with ________________
    no lines with a first digit and then more than 8 digits:
    this is to get rid of the date code like 03/04/2020
    since it does not correspond to any rank or other signoff,
    it's just the date of the report
    no lines that start with "Troop" unless they have  "Guide"
    """

    pat = re.compile(r"\d+/\d+/\d{4}")
    return all(
        (bool(line), not line.startswith("Adult Personal Data"), not pat.match(line))
    )


def get_good_lines(file_obj):
    """take all the lines and reject the ones that are not needed in the data"""
    return (
        line
        for page in file_obj
        for line in page.get_textbox(Config.WHOLE_PAGE).split("\n")
        if good_line(line)
    )


def parse_file(file):
    """parse the file and get the merit badges and names"""
    with fitz.open(file) as in_file:
        adults = list(get_adults(in_file))
    pprint(adults)


if __name__ == "__main__":
    parse_file(file)
