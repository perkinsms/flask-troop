from app.extensions import db
from app.config import Config
from app.models import (
    Scout,
    MBSignoff,
    MBRequirement,
    MeritBadge,
)

from app.utils import parse_name
from app.utils.logging import logger, log_it

from tm_parser.tm_parser import Parser


mb_file = Config.FILES["merit_badges"]
mb_names = Config.FILES["merit_badge_names"]


@log_it
def create_badge(mb_name):
    b = db.session.query(MeritBadge).filter(MeritBadge.name == mb_name).one_or_none()
    if not b:
        b = MeritBadge(name=mb_name)
        db.session.add(b)
    mbr = get_mb_requirement(mb_name)
    return b, mbr


def get_mb(mb_name, version=None, eagle_required=False):
    b = db.session.query(MeritBadge).filter(MeritBadge.name == mb_name).one_or_none()
    if not b:
        b = MeritBadge(
            name=mb_name,
            eagle_required=eagle_required,
            version=version,
        )
        db.session.add(b)
        db.session.commit()
    else:
        b.eagle_required = eagle_required
        b.version = version
    return b


def get_mb_requirement(mb_name):
    mbr = (
        db.session.query(MBRequirement)
        .filter(MBRequirement.badge_name == mb_name)
        .one_or_none()
    )
    if not mbr:
        mbr = MBRequirement(
            badge=get_mb(mb_name),
            code=f"{mb_name}-Completion",
            shortcode="Completion",
        )
        db.session.add(mbr)
        db.session.commit()
    return mbr


def get_mb_signoff(scout, requirement, date=None):
    mb_signoff = (
        db.session.query(MBSignoff)
        .filter(MBSignoff.scout_id == scout.id)
        .filter(MBSignoff.code == requirement.code)
        .one_or_none()
    )
    if not mb_signoff:
        mb_signoff = MBSignoff(scout=scout, requirement=requirement, date=date)
        db.session.add(mb_signoff)
        db.session.commit()
    else:
        mb_signoff.date = date
    return mb_signoff


def import_merit_badges(file):
    p = Parser(infile=file, outfile=None, file_format="json")
    for name, scout_data in p:
        scoutname = parse_name(name)
        s = Scout.named(
            lastname=scoutname.get("lastname", ""),
            firstname=scoutname.get("firstname", ""),
        )
        if not s:
            logger.warn(f"No scout item found for {name}")

        print(f"Importing {name}")
        logger.debug(f"importing scout {name}")
        if "Merit Badges" in scout_data:
            data = scout_data.get("Merit Badges")
        else:
            continue
        for badge_name, badge_data in data.items():
            badge = MeritBadge.named(badge_name)
            if not badge:
                logger.warn(f"No merit badge item found for {badge_name}")

            mbr = get_mb_requirement(badge_name)
            signoff = get_mb_signoff(
                scout=s, requirement=mbr, date=badge_data.get("date")
            )
    db.session.commit()


def make_badges_from_file(file):
    with open(file, newline="\n") as f:
        for line in f:
            if not line:
                continue
            print(f"Creating Badge: {line}")
            create_badge(line.strip())


def main():
    make_badges_from_file(mb_names)
    import_merit_badges(Config.FILES["tm_data"])
    db.session.commit()


if __name__ == "__main__":
    main()
