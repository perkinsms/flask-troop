"""
troopmaster/load_signoffs.py

loads signoffs from troopmaster file
"""

from app.extensions import db
from app.models import (
    RankRequirement,
    RankSignoff,
    Scout,
)

from app.utils.logging import logger, log_it
from app.utils.people import parse_name

from app.config import Config
from tm_parser.tm_parser import Parser
from tm_parser.tm_parser import Config as TM_CONFIG


@log_it
def signoff_item(scout, code, date):
    if not date:
        return
    requirement = RankRequirement.get(code=code)
    if not requirement:
        logger.error(f"in import_signoffs: could not find requirement {code}")
        return None
    signoff = (
        db.session.query(RankSignoff)
        .filter(RankSignoff.scout_id == scout.id)
        .filter(RankSignoff.code == requirement.code)
    ).one_or_none()
    if not signoff:
        print(f"Signing off item {code} for {scout} on {date}")
        signoff = RankSignoff(
            scout=scout, badge=requirement.badge, code=requirement.code
        )
        db.session.add(signoff)
    signoff.date = date
    return signoff


def import_signoffs(file):
    p = Parser(infile=file, outfile=None, file_format="toml")

    for scout, data in p:
        name = parse_name(scout)
        s = Scout.named(
            lastname=name.get("lastname", ""), firstname=name.get("firstname", "")
        )
        if not s:
            logger.error(f"in import_signoffs: could not find scout {scout}")
        if "Rank Advancement" in data:
            for rank, signoffs in data["Rank Advancement"].items():
                logger.debug(f"in import_signoffs: rank: {rank}")
                for code, date in signoffs.items():
                    if rank in TM_CONFIG.UPPER_RANKS and "MB" not in code:
                        code = getattr(TM_CONFIG, rank)[code]
                    signoff_item(scout=s, code=f"{rank}-{code}", date=date)


def main():
    import_signoffs(Config.FILES["tm_data"])


if __name__ == "__main__":
    main()
