"""
load_scouts.py

loads scouts from text file

depends:
load_ranks
load_groups

usage:
import load_scouts
load_scouts.main()
"""
import csv
from tqdm import tqdm

from app.extensions import db
from app.config import Config

from app.models import Scout, Group, Rank, User
from app.utils import parse_name
from app.utils.logging import log_it

scouts_file = Config.FILES["scouts"]

"""
Header from Troopwebhost Scout Directory

Name,
Mailing Address Line 1,
Mailing Address Line 2,
City,
State,
Zip Code,
Home Phone,
Cell Phone,
Email,
Email #2,
SMS,
Age,
Grade,
Rank,
Patrol,
Leadership
"""

SIGNING_POSITIONS = [
    "Troop Guide",
    "Patrol Leader",
    "ASPL",
    "Senior Patrol Leader",
    "Instructor",
]


def can_sign(positions):
    return any((position in positions for position in SIGNING_POSITIONS))


def create_username(text):
    name = parse_name(text)

    lastname = name["lastname"]
    firstname = name["firstname"]
    maybe_username = f"{lastname}{firstname}"
    for i in range(5, len(maybe_username)):
        username = (
            db.session.query(User)
            .filter(User.username == maybe_username[:i])
            .one_or_none()
        )
        if not username:
            return maybe_username[:i]


def email_is_unique(text):
    return not db.session.query(User).filter(User.email == text)


@log_it
def add_scout(line):
    name = parse_name(line)
    scout = Scout(
        lastname=name.get("lastname", ""),
        firstname=name.get("firstname"),
        middlename=name.get("middlename"),
    )
    db.session.add(scout)
    return scout


def add_more_information(scout, line):
    if scout.patrol and scout.patrol != line.get("Patrol"):
        scout.patrol.members.delete(scout)
    scout.patrol = line.get("Patrol")

    if not line.get("Email") or line.get("Email #2"):
        scout.email = None
    elif email_is_unique(line.get("Email")):
        scout.email = line.get("Email")
    elif email_is_unique(line.get("Email #2")):
        scout.email = line.get("Email #2")
    else:
        scout.email = line.get("Email") + "_scout"

    scout.username = create_username(line.get("Name"))
    scout.age = line.get("Age")
    # default rank is no rank
    scout.current_rank = Rank.named("No Rank")
    if scout.current_rank > Rank.named("Second Class") and can_sign(
        line.get("Leadership")
    ):
        scout.rank_signer = True


def add_scout_to_patrol(scout, patrol):
    if scout not in patrol.members:
        patrol.members.append(scout)


def import_scouts(file):
    with open(file, encoding="utf-8-sig") as f:
        reader = csv.DictReader(f)
        lines = len(f.readlines()) - 1
        f.seek(0)
        for line in tqdm(reader, total=lines, desc="Scouts"):
            group = Group.get_group(line.get("Patrol"))
            name = parse_name(line.get("Name"))
            if scout := Scout.get_scout(
                lastname=name.get("lastname"), firstname=name.get("firstname")
            ):
                ...
            else:
                scout = add_scout(line.get("Name"))
            add_more_information(scout, line)
            add_scout_to_patrol(scout, group)
        db.session.commit()


def main():
    import_scouts(scouts_file)


if __name__ == "__main__":
    main()
