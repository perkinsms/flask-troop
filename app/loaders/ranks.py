"""
load_ranks.py

loads ranks from ranks.csv
prerequisites:none

usage:
import load_ranks
load_ranks.main()
"""
import toml
from tqdm import tqdm

from app.extensions import db
from app.config import Config
from app.models import Rank
from app.utils.logging import log_it


RANKS_FILE = Config.FILES["requirements"]


@log_it
def add_rank(rank, rank_data):
    rank = Rank(
        name=rank,
        rank_order=rank_data["rank_order"],
        upper_rank=rank_data["upper_rank"],
        extra_rank=rank_data["extra_rank"],
    )
    db.session.add(rank)
    return rank


def import_ranks(file):
    data = toml.load(file)
    for rank, rank_data in tqdm(
        data.items(), total=len(list(data.items())), desc="Ranks"
    ):
        if not db.session.query(Rank).filter(Rank.name == rank).one_or_none():
            add_rank(rank, rank_data)
            db.session.commit()


def main():
    import_ranks(RANKS_FILE)


if __name__ == "__main__":
    main()
