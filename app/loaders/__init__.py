"""loaders/__init__.py
used to load all ranks, requirements, scouts, groups, signoffs from text files

usage:
import loaders

loaders.all()

"""
from dotenv import dotenv_values
from app.extensions import db
from app.utils.logging import logger
from app.config import Config
from . import (
    ranks,
    requirements,
    requirement_groups,
    groups,
    scouts,
    signoffs,
    preprocess,
    adults,
    merit_badges,
)


TROOPMASTER = Config.TROOPMASTER
if TROOPMASTER:
    from .troopmaster import (
        scouts as tm_scouts,
        signoffs as tm_signoffs,
        groups as tm_groups,
        merit_badges as tm_merit_badges,
    )

TROOP = Config.TROOP


def do_preprocess():
    logger.info("preprocessing file")
    print("preprocessing file")
    preprocess.main()
    logger.info("Done!")


def load_ranks():
    logger.info("Loading Ranks . . . ")
    print("Loading Ranks . . . ")
    ranks.main()
    db.session.commit()
    logger.info("Done!")


def load_adults():
    logger.info("Loading Adults . . . ")
    print("Loading Adults . . . ")
    adults.main()
    db.session.commit()
    logger.info("Done!")


def load_requirements():
    logger.info("Loading Requirements . . . ")
    print("Loading Requirements . . . ")
    requirements.main()
    db.session.commit()
    logger.info("Done!")


def load_scouts():
    logger.info("Loading Scouts . . . ")
    print("Loading Scouts . . . ")
    scouts.main()
    db.session.commit()
    logger.info("Done!")


def load_tm_scouts():
    logger.info("Loading Scouts . . . ")
    print("Loading Scouts . . . ")
    tm_scouts.main()
    db.session.commit()
    logger.info("Done!")


def load_tm_groups():
    logger.info("Loading Groups . . . ")
    print("Loading Groups . . . ")
    tm_groups.main()
    db.session.commit()
    logger.info("Done!")


def load_signoffs():
    logger.info("Loading Signoffs . . . ")
    print("Loading Signoffs . . . ")
    signoffs.main()
    db.session.commit()
    logger.info("Done!")


def load_tm_signoffs():
    logger.info("Loading Signoffs . . . ")
    print("Loading Signoffs . . . ")
    tm_signoffs.main()
    db.session.commit()
    logger.info("Done!")


def load_mb_completions():
    logger.info("Loading Merit Badge Completions . . . ")
    print("Loading Merit Badge Completions . . . ")
    merit_badges.main()
    db.session.commit()
    logger.info("Done!")


def load_tm_mb_completions():
    logger.info("Loading Merit Badge Completions . . . ")
    print("Loading Merit Badge Completions . . . ")
    tm_merit_badges.main()
    db.session.commit()
    logger.info("Done!")


def load_requirement_groups():
    logger.info("Loading Requirement Groups . . . ")
    print("Loading Requirement Groups . . . ")
    requirement_groups.main()
    db.session.commit()
    logger.info("Done!")


def variable():
    logger.info(f"Loading {TROOP} Variable . . . ")
    print(f"Loading {TROOP} Variable . . . ")

    load_signoffs()
    load_mb_completions()
    logger.info("All Done!")


def all():
    logger.info(f"Loading {TROOP} . . . ")
    print(f"Loading {TROOP} . . . ")

    if TROOPMASTER:
        load_ranks()
        load_requirements()
        load_tm_groups()
        load_tm_scouts()
        load_tm_signoffs()
        load_tm_mb_completions()
        load_requirement_groups()
    else:
        load_ranks()
        load_adults()
        load_requirements()
        load_scouts()
        load_signoffs()
        load_mb_completions()
        load_requirement_groups()
    logger.info("All Done!")
    print("All Done!")


if __name__ == "__main__":
    all()


__all__ = [
    "dotenv_values",
    "groups",
]
