"""
load_signoffs.py

loads signoffs from rank_requirements file
prerequisites
load_ranks
load_scouts
load_requirements
"""
import csv

from tqdm import tqdm

from app.extensions import db
from app.models import (
    Scout,
    RankSignoff,
    Requirement,
    Rank,
)

from app.utils import (
    parse_code,
    get_date,
    parse_name,
)
from app.utils.logging import logger, log_it

from app.config import Config


requirements_file = Config.FILES["scout_signoffs"]


@log_it
def add_signoff(scout, requirement, date):
    if (
        signoff := db.session.query(RankSignoff)
        .filter(RankSignoff.scout_id == scout.id)
        .filter(RankSignoff.code == requirement.code)
        .one_or_none()
    ):
        signoff.date = date
    else:
        signoff = RankSignoff(
            scout=scout, badge=requirement.badge, code=requirement.code, date=date
        )
        db.session.add(signoff)
    return signoff


def import_signoffs(file):
    with open(file, encoding="utf-8-sig") as f:
        reader = csv.DictReader(f)
        lines = len(f.readlines())
        f.seek(0)

        for line in tqdm(reader, total=lines, desc="Signoffs"):
            name = parse_name(line["Scout"])
            if not line["Date Earned"]:
                continue
            # print( f"importing signoff {line['Rank']} - {line['Code']} - {line['Scout']}")
            rank = Rank.named(line["Rank"])
            code = parse_code(line["Code"])
            r = Requirement.get(code=f"{line['Rank']}-{code}")
            s = Scout.named(
                lastname=name.get("lastname", ""), firstname=name.get("firstname", "")
            )
            if not s:
                print(f"warning: {line['Scout']} is new, get scouts.csv and reload")
                continue
            s.current_rank = Rank.named(line["Current Rank"]) or Rank.named("No Rank")
            logger.debug(
                f"Scout: {s.firstname} {s.lastname} Current Rank {s.current_rank}"
            )

            date = get_date(line["Date Earned"])
            add_signoff(s, r, date)
        db.session.commit()


def main():
    import_signoffs(requirements_file)


if __name__ == "__main__":
    main()
