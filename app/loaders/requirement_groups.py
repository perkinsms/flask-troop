"""
load_requirement_groups.py

loads ranks from requirement_groups.yaml
prerequisites:none

usage:
import load_requirement_groups
load_ranks.main()
"""
import toml
from tqdm import tqdm

from app.config import Config
from app.extensions import db
from app.models import (
    Rank,
    RankRequirement,
    MBRequirement,
    MBRGroup,
    RankRGroup,
)
from app.utils.logging import logger, log_it


files = Config.FILES["rgroups"], Config.FILES["custom_rgroups"]


@log_it
def load_rank_requirement_group(preset, data):
    p = db.session.query(RankRGroup).filter(RankRGroup.name == preset).one_or_none()
    if not p:
        p = RankRGroup(requirements=[], name=preset)
    else:
        p.requirements = []
    for badge_name, code_strings in tqdm(
        data["requirements"].items(), total=len(data["requirements"]), desc=preset
    ):
        badge = Rank.named(badge_name)
        if not badge:
            logger.warn(f"No such badge: {badge_name}")
            return None
        for code_string in code_strings:
            requirement = RankRequirement.get(f"{badge_name}-{code_string}")
            if not requirement:
                logger.warn(f"No such code: {code_string}")
                return None
            p.requirements.append(requirement)
    return p


@log_it
def load_mb_group(preset, data):
    p = db.session.query(MBRGroup).filter(MBRGroup.name == preset).one_or_none()
    if not p:
        p = MBRGroup(requirements=[], name=preset)
    else:
        p.requirements = []
    for badge in tqdm(data["badges"], total=len(data["badges"]), desc=preset):
        p.requirements.append(MBRequirement.get(f"{badge}-Completion"))
    return p


def main():
    presets = {}
    try:
        for file in files:
            presets |= toml.load(file)
    except FileNotFoundError:
        pass

    for preset, data in presets.items():
        if data["type"] == "rank":
            p = load_rank_requirement_group(preset, data)
        elif data["type"] == "merit_badge":
            p = load_mb_group(preset, data)
        db.session.add(p)
        db.session.commit()


if __name__ == "__main__":
    main()
