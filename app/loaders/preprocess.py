import pandas as pd
from app.utils import parse_code
from app.utils.logging import logger
from app.config import Config


# def bsa_files():
#    df = pd.read_csv(Config.RANK_REQUIREMENTS_FILE)
#    df.groupby(["Rank", "Code", "Requirement"])["Rank"].all().to_csv(
#        Config.REQUIREMENTS_FILE
#    )


def main():
    logger.info("processing scout signoffs file . . . ")
    print("processing scout signoffs file . . . ")
    df = pd.read_csv(Config.FILES["scout_signoffs"])

    df["Patrol"].fillna("No Patrol", inplace=True)
    df["Current Rank"].fillna("No Rank", inplace=True)
    df["Age"].fillna(0, inplace=True)

    df["Code"] = df.applymap(parse_code)["Code"]

    logger.info("Writing patrols file . . . ")
    print("Writing patrols file . . . ")
    df.groupby(["Patrol"])["Patrol"].all().to_csv(Config.FILES["groups"])

    logger.info("Writing scouts file . . . ")
    print("Writing scouts file . . . ")
    df.groupby(["Scout", "Patrol", "Age", "Current Rank"])["Scout"].all().to_csv(
        Config.FILES["scouts"],
        columns=["Name", "Patrol", "Age", "Current Rank", "Scout"],
    )


if __name__ == "__main__":
    main()
