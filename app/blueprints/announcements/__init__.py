from flask import Blueprint

bp = Blueprint("announcements", __name__)

from app.blueprints.announcements import routes

__all__ = [
    "bp",
    "routes",
]
