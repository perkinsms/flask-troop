from flask import (
    render_template,
    request,
)
from datetime import datetime
from app.blueprints.announcements import bp
from app.models import Announcement
from app.extensions import db


@bp.route("/get-all-cards")
def get_all_cards():
    announcements = Announcement.all()
    return render_template("announcements/cards.html", announcements=announcements)


@bp.route("/edit-announcement/<id>")
def edit(id):
    pass


@bp.route("/add", methods=["GET", "POST"])
def add():
    if request.method == "POST":
        a = Announcement(
            start_date=datetime.strptime(request.form["start-date"], "%Y-%m-%d").date(),
            end_date=datetime.strptime(request.form["end-date"], "%Y-%m-%d").date(),
            text=request.form["announcement-title"],
        )
        db.session.add(a)
        db.session.commit()
    else:
        return render_template("announcements/input.html")
