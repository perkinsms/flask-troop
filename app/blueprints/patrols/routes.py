from flask import render_template
from app.blueprints.patrols import bp
from app.models import Group, Requirement, Rank


@bp.route("/")
def index():
    return render_template("patrols/all.html", patrols=Group.get_patrols())


@bp.route("/by-name/<name>")
def by_name(name):
    patrol = Group.named(name)
    ranks = [r for r in Rank.all() if not r.extra_rank]
    requirements = sorted([r for r in Requirement.all() if r.badge in ranks])
    return render_template(
        "patrols/patrol.html", patrol=patrol, requirements=requirements
    )
