from flask import Blueprint

bp = Blueprint("patrols", __name__)

from app.blueprints.patrols import routes

__all__ = [
    "bp",
    "routes",
]
