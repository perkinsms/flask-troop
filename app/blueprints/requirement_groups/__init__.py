from flask import Blueprint

bp = Blueprint("requirement_groups", __name__)

from app.blueprints.requirement_groups import routes

__all__ = [
    "bp",
    "routes",
]
