import urllib
from flask import render_template, request, flash, redirect, url_for
from flask_login import login_required
from app.blueprints.requirement_groups import bp
from app.extensions import db
from app.models import RankRGroup, RankRequirement


@bp.route("/", methods=["GET"])
def all():
    rgroups = RankRGroup.all()
    return render_template("requirement_groups/all.html", rgroups=rgroups)


@login_required
@bp.route("/<string:rgroup_name>/edit", methods=["PATCH"])
def edit(rgroup_name):
    rgroup = RankRGroup.named(urllib.parse.unquote(rgroup_name))
    requirements = RankRequirement.all()
    return render_template(
        "requirement_groups/one_group.html", rgroup=rgroup, requirements=requirements
    )


@login_required
@bp.route("/<string:rgroup_name>/clone", methods=["POST"])
def clone(rgroup_name):
    rgroup = RankRGroup.named(urllib.parse.unquote(rgroup_name))
    new_rgroup_name = urllib.parse.unquote(request.headers.get("HX-Prompt"))
    if RankRGroup.exists(new_rgroup_name):
        flash(f"requirement_group already exists with name {new_rgroup_name}")
        return redirect(url_for("groups.all"))
    new_rgroup = RankRGroup(name=new_rgroup_name)
    new_rgroup.requirements = rgroup.requirements.copy()
    db.session.add(new_rgroup)
    db.session.commit()
    return redirect(url_for("requirement_groups.all"))


@login_required
@bp.route("/<string:rgroup_name>/delete", methods=["DELETE"])
def delete(rgroup_name):
    rgroup = RankRGroup.named(urllib.parse.unquote(rgroup_name))
    del rgroup
    db.session.commit()
    return "BALEETED"


@login_required
@bp.route("/add", methods=["PUT"])
def add():
    rgroup_name = urllib.parse.unquote(request.headers.get("HX-Prompt"))
    if RankRGroup.exists(rgroup_name):
        flash(f"requirement group already exists with name {rgroup_name}")
        return redirect(url_for("requirement_groups.all"))
    new_rgroup = RankRGroup(name=rgroup_name)
    db.session.add(new_rgroup)
    db.session.commit()
    return redirect(url_for("requirement_groups.all"))


@login_required
@bp.route("/<string:rgroup_name>/requirement_delete/<string:code>", methods=["DELETE"])
def requirement_delete(rgroup_name, code):
    rgroup = RankRGroup.named(urllib.parse.unquote(rgroup_name))
    requirement = RankRequirement.get(code)
    if requirement in rgroup.requirements:
        rgroup.requirements.remove(requirement)
        db.session.commit()
        return "DELETED"
    return "ERROR"


@login_required
@bp.route("/<string:rgroup_name>/requirement_add/<string:code>", methods=["PUT"])
def requirement_add(rgroup_name, code):
    rgroup = RankRGroup.named(urllib.parse.unquote(rgroup_name))
    requirement = RankRequirement.get(code)
    rgroup.requirements.append(requirement)
    db.session.commit()
    return ":thumbsup:"
