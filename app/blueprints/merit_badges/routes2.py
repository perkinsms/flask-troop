from flask import render_template, request

from app.blueprints.merit_badges import bp
from app.models import Group, MeritBadge, MBRGroup, MBRequirement, Scout
from app.utils.logging import logger


@bp.route("/blankrow", methods=["DELETE"])
def delete_row():
    return ""


@bp.route("/", methods=["POST", "GET"])
def index():
    if request.method == "GET":
        return initial_page()
    elif request.method == "POST":
        return results_page()


def initial_page():
    return render_template(
        "merit_badges/all.html",
        options={
            "scouts": sorted(Scout.all()),
            "patrols": sorted(Group.all()),
            "presets": sorted(MBRGroup.all()),
            "badges": sorted(MeritBadge.all()),
            "requirements": sorted(MBRequirement.all()),
        },
        selected={
            "scouts": [],
            "patrols": [],
            "presets": [],
            "badges": [],
            "requirements": [],
        },
        displayed={
            "scouts": [],
            "patrols": [],
            "requirements": [],
        },
    )


def results_page():
    selected = get_selected(request)
    displayed = get_displayed(selected)

    logger.debug(sorted(MBRequirement.all()))

    options = {
        "scouts": sorted(Scout.all()),
        "patrols": sorted(Group.all()),
        "presets": sorted(MBRGroup.all()),
        "badges": sorted(MeritBadge.all()),
        "requirements": sorted(MBRequirement.all()),
    }

    selected = {
        "scouts": selected["scout_ids"],
        "patrols": selected["patrol_names"],
        "presets": selected["preset_names"],
        "badges": selected["merit_badge_names"],
        "requirements": selected["codes"],
    }

    return render_template(
        "merit_badges/all.html",
        troop_page=not selected["patrols"],
        options=options,
        selected=selected,
        displayed=displayed,
    )


def get_selected(request):
    patrol_names = set(request.form.getlist("patrols"))
    patrol_scout_ids = {
        scout.id for patrol in patrol_names for scout in Group.named(patrol).members
    }
    scout_ids = set(request.form.getlist("scouts"))
    preset_names = set(request.form.getlist("presets"))
    merit_badge_names = set(request.form.getlist("merit_badges"))
    mb_requirements = {
        merit_badge.code
        for merit_badge in merit_badge_names
        for requirement in MeritBadge.get(merit_badge).requirements
    }

    codes = set(request.form.getlist("requirements"))

    return {
        "patrol_names": sorted(patrol_names),
        "patrol_scout_ids": patrol_scout_ids,
        "scout_ids": scout_ids,
        "preset_names": sorted(preset_names),
        "merit_badge_names": sorted(merit_badge_names),
        "mb_requirement_codes": sorted(mb_requirements),
        "codes": codes,
    }


def get_displayed(selected):
    requirement_codes = (
        selected["codes"]
        or selected["mb_requirement_codes"]
        or {requirement.code for requirement in MBRequirement.all()}
    )

    scout_ids = (
        selected["scout_ids"]
        or selected["patrol_scout_ids"]
        or {scout.id for scout in Scout.all()}
    )

    if selected["preset_names"]:
        preset_requirements = {
            r.code
            for p in selected["preset_names"]
            for r in MBRGroup.named(p).requirements
        }
        requirement_codes.intersection_update(preset_requirements)

    requirements = sorted([MBRequirement.get(code) for code in requirement_codes])
    logger.info(requirements)
    scouts = [Scout.named(id=id) for id in scout_ids]
    patrols = [Group.named(name) for name in {scout.patrol for scout in scouts}]

    return {
        "patrols": sorted(patrols),
        "scouts": sorted(scouts),
        "requirements": sorted(requirements),
    }
