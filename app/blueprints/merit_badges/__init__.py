from flask import Blueprint

bp = Blueprint("merit_badges", __name__)

from app.blueprints.merit_badges import routes, routes2

__all__ = [
    "bp",
    "routes",
    "routes2",
]
