from datetime import datetime

from flask import render_template, request

from app.blueprints.merit_badges import bp
from app.extensions import db
from app.models import MBSignoff, Scout, Adult
from app.utils.logging import logger, requirements_logger


@bp.route("/<scout_id>/<code>/<date>/<signer_id>", methods=["GET", "POST", "DELETE"])
def sign(scout_id: int, code: str, date: str, signer_id: int):
    logger.debug((scout_id, code, date, signer_id, request.method))
    data = get_data(scout_id, code, date, signer_id, request)
    if "error" in data:
        logger.debug(data)
        return "WARN"

    # sign-off of a unchecked box
    if request.method == "POST":
        scout = Scout.named(id=scout_id)
        badge, shortcode = code.split("-")
        requirements_logger.info(
            f'"{scout.lastname}, {scout.firstname}",{scout.age},{scout.patrol},{scout.current_rank_name},{badge},{shortcode},,{data["date"]}'
        )
        data["signoff"].date = data["date"]
        db.session.commit()
        return render_template(
            "check_boxes/added.html",
            scout=data["scout"],
            requirement=data["signoff"].requirement,
            date=date,
            signer_id=signer_id,
        )

    # remove sign-off of a checked box or thumbs up
    if request.method == "DELETE":
        scout = Scout.named(id=scout_id)
        badge, shortcode = code.split("-")
        requirements_logger.info(
            f'"{scout.lastname}, {scout.firstname}",{scout.age},{scout.patrol},{scout.current_rank_name},{badge},{shortcode},,'
        )
        data["signoff"].date = None
        db.session.commit()
        return render_template(
            "check_boxes/removed.html",
            scout=data["scout"],
            requirement=data["signoff"].requirement,
            date=date,
            signer_id=signer_id,
        )

    if request.method == "GET":
        data["signoff"].date = data["date"]
        db.session.commit()
        return {"status": "ok"}


def get_data(scout_id, code, date, signer_id, request):
    if not Scout.named(id=scout_id):
        return {"status": "not ok", "error": "invalid scout"}
    scout = Scout.named(id=scout_id)
    if not Adult.get(id=signer_id):
        return {"status": "not ok", "error": "invalid adult"}
    adult = Adult.named(id=signer_id)
    if not adult.signer:
        return {"status": "not ok", "error": "invalid signer"}
    if not MBSignoff.get(scout.id, code):
        return {"status": "not ok", "error": "invalid badge or code"}
    signoff = MBSignoff.get(scout.id, code)
    if request.method == "POST":
        if signoff.date:
            return {"status": "not ok", "error": f"already signed on {signoff.date}"}
    if date == "Today":
        date = datetime.today().strftime("%Y-%m-%d")
    date = datetime.strptime(date, "%Y-%m-%d").date()
    return {
        "status": "ok",
        "scout": scout,
        "signer": adult,
        "signoff": signoff,
        "date": date,
    }
