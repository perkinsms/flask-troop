from flask import Blueprint

bp = Blueprint("scouts", __name__)

from app.blueprints.scouts import routes

__all__ = [
    "bp",
    "routes",
]
