from operator import attrgetter
from flask import render_template, render_template_string, request
from app.blueprints.scouts import bp
from app.models import Scout, Group, Rank
from itertools import chain


@bp.route("/")
def index():
    return render_template(
        "scouts/all.html",
        scouts=sorted(
            [(scout, scout.sorted_signoffs()) for scout in Scout.all()],
            key=lambda x: x[0].patrol,
        ),
    )


@bp.route("/by_id/<id>")
def by_id(id):
    scout = Scout.get_scout(id=id)
    ranks = Rank.all()
    if scout.current_rank.upper_rank or scout.current_rank_name == "First Class":
        return render_template("scouts/scout_upper.html", scout=scout, ranks=ranks)
    else:
        return render_template("scouts/scout_lower.html", scout=scout, ranks=ranks)


@bp.route("/scout-selector")
def scout_selector():
    template = """
{% for scout in scouts %}
<option value="{{ scout.id }}">{{ scout.display_name }}</option>
{% endfor %}
"""
    if not request.args.getlist("patrols"):
        scouts = Scout.all()
    else:
        scouts = chain.from_iterable(
            [Group.named(name).members for name in request.args.getlist("patrols")]
        )
        scouts = filter(lambda x: isinstance(x, Scout), scouts)
    return render_template_string(
        template, scouts=sorted(scouts, key=attrgetter("display_name"))
    )
