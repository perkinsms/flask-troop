from pprint import pprint
from flask import (
    render_template,
    request,
    url_for,
    redirect,
    flash,
)
from flask_login import (
    current_user,
    login_user,
    logout_user,
    login_required,
)

from werkzeug.security import (
    generate_password_hash,
    check_password_hash,
)

from app.blueprints.users import bp
from app.models import User
from app.extensions import db


@bp.route("/register")
def register():
    """Give them the registration box"""
    return render_template("users/register.html")


@bp.route("/register", methods=["POST"])
def register_post():
    """Validate the user registration and put them in the database"""
    email = request.form.get("email", "")
    username = request.form.get("username", "")
    password = request.form.get("password", "")
    user = db.session.query(User).filter(User.email == email).one_or_none()

    if not user:
        flash("No User with that email or username Exists")
        return redirect(url_for("users.register"))
    if user.registered:
        flash("Already Registered")
        return redirect(url_for("users.login"))
    user.hashed_password = generate_password_hash(password)
    user.registered = True
    user.username = username
    db.session.commit()
    flash(f"User {username} registered with email {email}")

    login_user(user)

    return redirect(url_for("main.index"))


@bp.route("/login")
def login():
    return render_template("users/login.html")


@bp.route("/login", methods=["POST"])
def login_post():
    email = request.form.get("email")
    password = request.form.get("password")
    user = (
        db.session.query(User).filter(User.email == email).one_or_none()
        or db.session.query(User).filter(User.username == email).one_or_none()
    )
    if not user:
        flash("No such user or email")
        return redirect(url_for("users.login"))
    if not user.registered:
        flash("Not Registered")
        return redirect(url_for("users.register"))
        return
    if check_password_hash(user.hashed_password, password):
        username = user.username
        login_user(user)
        flash(f"User {username} logged in")
        return redirect(url_for("main.index"))
    return "Invalid Password"


@login_required
@bp.route("/logout")
def logout():
    flash("user logged out")
    logout_user()
    return redirect(url_for("main.index"))


@login_required
@bp.route("/profile")
def profile():
    user = current_user
    return render_template("users/profile.html", user=user)


@login_required
@bp.route("/edit/<user_id>")
def edit_user_form(user_id):
    return render_template("users/edit_form.html", user=user_id)


@login_required
@bp.route("/edit/<user_id>", methods=["POST"])
def edit_user(user_id):
    pprint(dir(request))
    user = User.get(id=user_id)
    username = request.form.get("username")
    print(username)
    email = request.form.get("email")
    print(email)
    u = db.session.query(User).filter(User.username == username).one_or_none()
    if u:
        flash("Username Taken, please choose another")
        return redirect(url_for("users.profile"))
    flash("Success")
    user.username = username
    user.email = email
    db.session.commit()
    return redirect(url_for("users.profile"))
