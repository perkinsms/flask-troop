from flask import Blueprint

bp = Blueprint("advancement", __name__)

from app.blueprints.advancement import routes

__all__ = [
    "bp",
    "routes",
]
