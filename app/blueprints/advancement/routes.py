from flask import (
    render_template,
    render_template_string,
    request,
)
from itertools import chain

from app.blueprints.advancement import bp
from app.models import (
    Group,
    Rank,
    RankRGroup,
    Requirement,
    RankRequirement,
    Scout,
)


@bp.route("/blankrow", methods=["DELETE"])
def delete_row():
    return ""


@bp.route("/", methods=["POST", "GET"])
def index():
    if request.method == "GET":
        return initial_page()
    elif request.method == "POST":
        return results_page()


@bp.route("/requirement-selector")
def requirement_selector():
    template = """
{% for requirement in requirements %}
<option value="{{ requirement.code}}">
{{ requirement.badge.name }}, {{requirement.shortcode}}, {{requirement.text}}
    </option>
    {% endfor %}
"""

    if not request.args.getlist("ranks"):
        requirements = Requirement.all()
    else:
        requirements = chain.from_iterable(
            [Rank.named(name).requirements for name in request.args.getlist("ranks")]
        )
    return render_template_string(template, requirements=sorted(requirements))


def initial_page():
    return render_template(
        "advancement/all.html",
        options={
            "scouts": sorted(Scout.all()),
            "patrols": sorted(Group.all()),
            "presets": sorted(RankRGroup.all()),
            "ranks": Rank.all()[1:],
            "requirements": sorted(RankRequirement.all()),
        },
        selected={
            "scouts": [],
            "patrols": [],
            "presets": [],
            "ranks": Rank.all()[1:5],
            "requirements": [],
        },
        displayed={
            "scouts": [],
            "patrols": [],
            "requirements": [],
        },
    )


def results_page():
    selected = get_selected(request)
    displayed = get_displayed(selected)
    by_patrol = request.form.get("by_patrol", False)


    options = {
        "scouts": sorted(Scout.all()),
        "patrols": sorted(Group.all()),
        "presets": sorted(RankRGroup.all()),
        "ranks": Rank.all()[1:],
        "requirements": sorted(RankRequirement.all()),
    }

    selected = {
        "scouts": selected["scout_ids"],
        "patrols": selected["patrol_names"],
        "presets": selected["preset_names"],
        "ranks": selected["rank_names"],
        "requirements": selected["codes"],
    }

    return render_template(
        "advancement/all.html",
        troop_page=not by_patrol,
        options=options,
        selected=selected,
        displayed=displayed,
    )


def get_selected(request):
    patrol_names = set(request.form.getlist("patrols"))
    patrol_scout_ids = {
        scout.id
        for patrol in patrol_names
        for scout in Group.named(patrol).members
        if isinstance(scout, Scout)
    }
    scout_ids = set(request.form.getlist("scouts"))
    preset_names = set(request.form.getlist("presets"))
    rank_names = set(request.form.getlist("ranks"))
    rank_requirements = {
        requirement.code
        for rank in rank_names
        for requirement in Rank.named(rank).requirements
    }
    sort_requirements = request.form.get("requirements_sorted")
    by_patrol = request.form.get("by_patrol", False)

    codes = set(request.form.getlist("requirements"))

    return {
        "preset_names": preset_names,
        "patrol_names": patrol_names,
        "patrol_scout_ids": patrol_scout_ids,
        "scout_ids": scout_ids,
        "rank_names": rank_names,
        "rank_requirement_codes": rank_requirements,
        "codes": codes,
        "sort_requirements": sort_requirements,
    }


def get_displayed(selected):
    requirement_codes = (
        selected["codes"]
        or selected["rank_requirement_codes"]
        or {requirement.code for requirement in RankRequirement.all()}
    )

    if selected["preset_names"]:
        preset_requirements = {
            r.code
            for p in selected["preset_names"]
            for r in RankRGroup.named(p).requirements
        }
        requirement_codes.intersection_update(preset_requirements)

    scout_ids = (
        selected["scout_ids"]
        or selected["patrol_scout_ids"]
        or {scout.id for scout in Scout.all()}
    )

    requirements = [RankRequirement.get(code) for code in requirement_codes]
    max_rank = max([r.badge for r in requirements if r.type == "rank_requirement"])
    scouts = [
        Scout.named(id=id)
        for id in scout_ids
        if Scout.named(id=id).current_rank < max_rank
    ]
    patrols = [Group.named(name) for name in {scout.patrol for scout in scouts}]

    if selected.get("sort_requirements"):
        requirements = sorted(
            sorted(
                sorted(requirements, key=lambda x: x.code),
                key=lambda x: x.need_count(scouts),
                reverse=True,
            ),
            key=lambda x: x.badge,
        )
    else:
        requirements = sorted(requirements)

    return {
        "patrols": sorted(patrols),
        "scouts": sorted(scouts),
        "requirements": requirements,
    }
