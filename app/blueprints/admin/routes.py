from flask import render_template
from flask_login import login_required
from app.blueprints.admin import bp


@login_required
@bp.route("/")
def index():
    return render_template("admin/index.html")
