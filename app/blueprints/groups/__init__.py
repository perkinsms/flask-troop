from flask import Blueprint

bp = Blueprint("groups", __name__)

from app.blueprints.groups import routes

__all__ = [
    "bp",
    "routes",
]
