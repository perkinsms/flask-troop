import urllib
from flask import render_template, request, flash, redirect, url_for
from flask_login import login_required
from app.blueprints.groups import bp
from app.extensions import db
from app.models import Group, User, Scout, Adult


@bp.route("/", methods=["GET"])
def all():
    groups = Group.all()
    return render_template("groups/all.html", groups=groups)


@login_required
@bp.route("/<string:group_name>/edit", methods=["PATCH"])
def edit(group_name):
    group = Group.named(urllib.parse.unquote(group_name))
    all_people = Scout.all() + Adult.all()
    return render_template("groups/one_group.html", group=group, users=all_people)


@login_required
@bp.route("/<string:group_name>/clone", methods=["POST"])
def clone(group_name):
    group = Group.named(urllib.parse.unquote(group_name))
    new_group_name = urllib.parse.unquote(request.headers.get("HX-Prompt"))
    if Group.exists(new_group_name):
        flash(f"group already exists with name {new_group_name}")
        return redirect(url_for("groups.all"))
    new_group = Group(name=new_group_name)
    new_group.members = group.members.copy()
    db.session.add(new_group)
    db.session.commit()
    return redirect(url_for("groups.all"))


@login_required
@bp.route("/<string:group_name>/delete", methods=["DELETE"])
def delete(group_name):
    group = Group.named(urllib.parse.unquote(group_name))
    del group
    db.session.commit()
    return "BALEETED"


@login_required
@bp.route("/add", methods=["PUT"])
def add():
    group_name = urllib.parse.unquote(request.headers.get("HX-Prompt"))
    if Group.exists(group_name):
        flash(f"group already exists with name {group_name}")
        return redirect(url_for("groups.all"))
    new_group = Group(name=group_name)
    db.session.add(new_group)
    db.session.commit()
    return redirect(url_for("groups.all"))


@login_required
@bp.route("/<string:group_name>/member_delete/<string:member_id>", methods=["DELETE"])
def member_delete(group_name, member_id):
    group = Group.named(urllib.parse.unquote(group_name))
    user = User.named(id=member_id)
    if user in group.members:
        group.members.remove(user)
        if hasattr(user, "patrol"):
            user.patrol = ""
        db.session.commit()
        return "DELETED"
    return "ERROR"


@login_required
@bp.route("/<string:group_name>/member_add/<string:user_id>", methods=["PUT"])
def member_add(group_name, user_id):
    group = Group.named(urllib.parse.unquote(group_name))
    user = User.named(id=user_id)
    group.members.append(user)
    db.session.commit()
    return ":thumbsup:"
