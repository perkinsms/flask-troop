from flask import Blueprint

bp = Blueprint("signoffs", __name__)

from app.blueprints.signoffs import routes

__all__ = [
    "bp",
    "routes",
]
