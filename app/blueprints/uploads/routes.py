from flask import (
    render_template,
    request,
    flash,
    redirect,
    current_app,
    url_for,
)
from pathlib import Path
from werkzeug.utils import secure_filename
from app.blueprints.uploads import bp
from tm_parser import Parser, dump_string
from json2html import json2html


ALLOWED_EXTENSIONS = {"pdf"}


def allowed_file(filename):
    return "." in filename and filename.rsplit(".", 1)[1].lower() in ALLOWED_EXTENSIONS


@bp.route("/", methods=["GET", "POST"])
def index():
    if request.method == "POST":
        if "file" not in request.files:
            flash("No File part")
            return redirect(request.url)
        file = request.files["file"]
        if file.filename == "":
            flash("no selected file")
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(Path(current_app.config["UPLOAD_FOLDER"]) / filename)
    return render_template("uploads/all.html")


@bp.route("/tm_scout_history_upload", methods=["POST"])
def tm_scout_history_upload():
    if "file" not in request.files:
        flash("No File Part")
        return redirect(url_for("uploads.index"))
    file = request.files["file"]
    if file.filename == "":
        flash("No Selected File")
        return redirect(url_for("uploads.index"))
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        file.save(Path(current_app.config["UPLOAD_FOLDER"]) / filename)
        parsed = Parser(
            infile=Path(current_app.config["UPLOAD_FOLDER"]) / filename,
            file_format="json",
        )

        data = []
        for scout, scout_data in parsed.scouts.items():
            data.append(
                {
                    "name": scout,
                    "data": json2html.convert(dump_string(scout_data, "json")),
                }
            )

        return render_template("uploads/tm_upload_confirm.html", data=data)


@bp.route("/confirm_tm_upload", methods=["POST"])
def confirm_tm_upload():
    if "confirm" in request.form:
        return "Confirmed"
    return "Cancelled"
