from flask import Blueprint

bp = Blueprint("uploads", __name__)

from app.blueprints.uploads import routes

__all__ = [
    "bp",
    "routes",
]
