import os
from pathlib import Path
from dotenv import dotenv_values

basedir = Path(os.path.dirname(__file__))


class Config:
    # get the troop from the environment or the .env file - default is "example_troop"
    TROOP = os.getenv("TROOP") or dotenv_values().get("TROOP", "example_troop")

    # the troop directory is data/TROOP/
    LOG_DIR = TROOP_DIR = Path(basedir).parent / "data" / TROOP

    UPLOAD_FOLDER = Path(TROOP_DIR) / "uploads"

    # set the TROOPMASTER variable if the environment is set,
    # the .env file says it
    # or the /data/TROOP/.env file says it

    TROOPMASTER = (
        os.getenv("troopmaster")
        or dotenv_values().get("troopmaster")
        or dotenv_values(Path(TROOP_DIR) / ".env").get("troopmaster", False)
    )

    # the secret key should be in the data/TROOP/.env
    SECRET_KEY = (
        os.getenv("SECRET_KEY")
        or dotenv_values().get("SECRET_KEY")
        or dotenv_values(Path(TROOP_DIR) / ".env").get("SECRET_KEY")
    )

    SQLALCHEMY_DATABASE_URI = os.environ.get("DATABASE_URI") or "sqlite:///" + str(
        Path(TROOP_DIR) / "app.db"
    )

    SQLALCHEMY_TRACK_MODIFICATIONS = False
    FILES = {
        "requirements": Path(TROOP_DIR) / "requirements.toml",
        "merit_badge_names": Path(TROOP_DIR) / "mb_names.txt",
        "rgroups": Path(TROOP_DIR) / "requirement_groups.toml",
        "custom_rgroups": Path(TROOP_DIR) / "custom_requirement_groups.toml" or None,
        "scouts": Path(TROOP_DIR) / f"{TROOP}_scouts.csv",
        "groups": Path(TROOP_DIR) / f"{TROOP}_patrols.csv",
        "adults": Path(TROOP_DIR) / f"{TROOP}_adults.csv",
        "merit_badges": Path(TROOP_DIR) / f"{TROOP}_merit_badges.csv",
        "scout_signoffs": Path(TROOP_DIR) / f"{TROOP}_rank_signoffs.csv",
        "mb_requirements": Path(TROOP_DIR) / f"{TROOP}_merit_badge_signoffs.csv",
    }

    if TROOPMASTER:
        FILES["tm_data"] = Path(TROOP_DIR) / "tm_data.pdf"
