"""
scout.py

"""
from collections import OrderedDict
from app.extensions import db
from .rank import Rank
from .adult import User
from .signoff import Signoff, MBSignoff
from app.utils import display_name


class Scout(User):
    __tablename__ = "scout"
    id = db.Column(db.Integer, db.ForeignKey("user.id"), primary_key=True)
    age = db.Column(db.String)
    patrol = db.Column(db.String)
    current_rank_name = db.Column(db.String, db.ForeignKey("badge.name"))
    current_rank = db.relationship("Badge")
    signoffs = db.relationship("Signoff")
    parents = db.relationship(
        "Adult", secondary="parent_child", back_populates="children"
    )

    __mapper_args__ = {
        "polymorphic_identity": "scout",
    }

    def __repr__(self):
        return f"<Scout {self.firstname} {self.lastname} ({self.current_rank})>"

    @classmethod
    def all(cls):
        return db.session.query(cls).all()

    @classmethod
    def get_scout(cls, firstname=None, lastname=None, id=None):
        if lastname and firstname:
            return (
                db.session.query(cls)
                .filter(cls.lastname == lastname)
                .filter(cls.firstname == firstname)
                .one_or_none()
            )
        elif id:
            return db.session.query(cls).filter(cls.id == id).one_or_none()

    named = get_scout

    def signoff(self, requirement):
        return getattr(self.get_signoff(requirement.code), "date", None)

    def signoff_needs(self, codes=None):
        if not codes:
            return {s.code for s in self.signoffs if not s.date}
        else:
            return {s.code for s in self.signoffs if s.code in codes and not s.date}

    def get_rank_date(self, rank):
        if rank.name == "No Rank":
            return "N/A"
        if rank.name == "Scout":
            if scout_signoffs := [
                signoff.date
                for signoff in self.signoffs
                if signoff.badge_name == "Scout"
            ]:
                return max(scout_signoffs)
            return "N/A"
        else:
            return getattr(self.get_signoff(rank.last_requirement().code), "date", None)

    @property
    def all_rank_dates(self):
        dates = OrderedDict()
        for rank in Rank.all()[1:]:
            date = self.get_rank_date(rank)
            if date:
                dates[rank.name] = date
        return dates

    @property
    def current_rank_date(self):
        if self.current_rank.name == "No Rank":
            return "N/A"
        if date := self.get_rank_date(self.current_rank):
            return date
        else:
            return "N/A"

    def get_signoff(self, code):
        return (
            db.session.query(Signoff)
            .join(Scout)
            .filter(Scout.id == self.id)
            .filter(Signoff.code == code)
            .one_or_none()
        )

    def get_mb_signoff(self, code):
        return (
            db.session.query(MBSignoff)
            .join(Scout)
            .filter(Scout.id == self.id)
            .filter(MBSignoff.code == code)
            .one_or_none()
        )

    def signoffs_for_rank(self, badge):
        return sorted([s for s in self.signoffs if s.badge_name == badge.name])

    def has_earned(self, rank):
        return bool(self.get_rank_date(rank))

    @property
    def pct_to_next_rank(self):
        next_rank = (
            db.session.query(Rank)
            .filter(Rank.rank_order == (self.current_rank.rank_order + 1))
            .one_or_none()
        )
        total_reqs = len(next_rank.requirements)

        finished_reqs = (
            len(
                [
                    signoff
                    for signoff in self.signoffs
                    if signoff.badge_name == next_rank.name
                ]
            )
            or 0
        )
        return float(finished_reqs / total_reqs * 100)

    @property
    def display_name(self):
        return display_name(self.lastname, self.firstname, self.middlename)
