"""
announcement.py

"""
from app.extensions import db
from markdown import markdown


class Announcement(db.Model):
    __tablename__ = "announcement"
    id = db.Column(db.Integer, primary_key=True)
    start_date = db.Column(db.Date)
    end_date = db.Column(db.Date)
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"))
    user = db.relationship("User")
    text = db.Column(db.String)
    priority = db.Column(db.Integer)

    def __repr__(self):
        date = self.start_date.strftime("%m/%d/%Y") if self.start_date else "N/A"
        return f"<Announcement {date} priority {str(self.priority)}>"

    def __lt__(self, other):
        if self.type == other.type:
            return (self.priority, self.start_date) < (
                other.priority,
                other.start_date,
            )
        return NotImplemented

    @classmethod
    def all(cls):
        """returns all announcements in database"""
        return db.session.query(cls).all()

    def get_html(self):
        """returns the converted html from the markdown for an annoucement"""
        return markdown(self.text)
