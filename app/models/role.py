"""
role.py

"""
from app.extensions import db


class Role(db.Model):
    __tablename__ = "role"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)

    # 4 + 2 + 1 = all, patrol, view
    advancement = db.Column(db.Integer)

    def __repr__(self):
        return f"<Role {self.name}>"

    @classmethod
    def all(cls):
        return db.session.query(cls).all()

    @classmethod
    def named(cls, name):
        return db.session.query(cls).filter(cls.name == name).one_or_none()


class UserRole(db.Model):
    __tablename__ = "user_role"
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"), primary_key=True)
    role_id = db.Column(db.Integer, db.ForeignKey("role.id"), primary_key=True)
