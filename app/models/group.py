"""
group.py

"""
from app.extensions import db


class Group(db.Model):
    __tablename__ = "group"
    name = db.Column(db.String, primary_key=True)
    members = db.relationship("User", secondary="user_group", back_populates="groups")

    def __repr__(self):
        return f"<Group: {self.name}>"

    def __lt__(self, other):
        return self.name < other.name

    def __iter__(self):
        for member in self.members:
            yield member

    @classmethod
    def exists(cls, name):
        return bool(db.session.query(cls).filter(cls.name == name).one_or_none())

    @classmethod
    def get_group(cls, name):
        group = db.session.query(cls).filter(cls.name == name).one_or_none()
        if not group:
            group = cls(name=name)
            db.session.add(group)
        return group

    named = get_group

    @classmethod
    def all(cls):
        return db.session.query(cls).all()

    get_patrols = all


class UserGroup(db.Model):
    __tablename__ = "user_group"
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"), primary_key=True)
    group_id = db.Column(db.String, db.ForeignKey("group.name"), primary_key=True)
