"""
rank.py

models a BSA rank

subclasses from Badge, which has name, version, last_requirement, and requirements

"""
from app.extensions import db
from .badge import Badge


class Rank(Badge):
    __tablename__ = "rank"
    upper_rank = db.Column(db.Boolean)
    extra_rank = db.Column(db.Boolean)
    rank_order = db.Column(db.Integer)

    __mapper_args__ = {
        "polymorphic_identity": "rank",
    }

    def __repr__(self):
        return f"<rank {self.name}>"

    def __index__(self):
        return self.rank_order

    def __lt__(self, other):
        return self.rank_order < other.rank_order

    def last_requirement(self):
        return max(self.requirements)
