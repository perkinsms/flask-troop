
from app.extensions import db


class RGroup(db.Model):
    __tablename__ = "rgroup"
    name = db.Column(db.String, primary_key=True)
    requirements = db.relationship(
        "Requirement", secondary="requirement_rgroup", back_populates="groups"
    )
    type = db.Column(db.String)

    __mapper_args__ = {
        "polymorphic_identity": "rgroup",
        "polymorphic_on": "type",
    }

    def __repr__(self):
        return f"<RGroup: {self.name}>"

    def __len__(self):
        return len(self.requirements)

    def __lt__(self, other):
        return self.name < other.name

    def __iter__(self):
        for i in self.requirements:
            yield i

    @classmethod
    def exists(cls, name):
        return bool(cls.named(name))

    @classmethod
    def named(cls, name):
        return db.session.query(cls).filter(cls.name == name).one_or_none()

    @classmethod
    def all(cls):
        return db.session.query(cls).all()


class RankRGroup(RGroup):
    __tablename__ = "rank_rgroup"

    __mapper_args__ = {
        "polymorphic_identity": "rank_rgroup",
    }


class MBRGroup(RGroup):
    __tablename__ = "mb_rgroup"

    __mapper_args__ = {
        "polymorphic_identity": "mb_rgroup",
    }


class RequirementRGroup(db.Model):
    __tablename__ = "requirement_rgroup"
    group = db.Column(db.String, db.ForeignKey("rgroup.name"), primary_key=True)
    requirement_code = db.Column(
        db.Integer, db.ForeignKey("requirement.code"), primary_key=True
    )
