"""
user.py

"""
from flask_login import UserMixin

from app.extensions import db
from app.utils import display_name


class User(UserMixin, db.Model):
    __tablename__ = "user"
    id = db.Column(db.Integer, primary_key=True)
    firstname = db.Column(db.String)
    lastname = db.Column(db.String)
    middlename = db.Column(db.String)
    username = db.Column(db.String, unique=True)
    hashed_password = db.Column(db.String)
    email = db.Column(db.String)
    rank_signer = db.Column(db.Boolean)
    roles = db.relationship("Role", secondary="user_role")
    registered = db.Column(db.Boolean)
    groups = db.relationship("Group", secondary="user_group", back_populates="members")
    type = db.Column(db.String)

    __mapper_args__ = {
        "polymorphic_identity": "user",
        "polymorphic_on": "type",
    }

    def __repr__(self):
        return f"<User {self.firstname} {self.lastname}>"

    def __lt__(self, other):
        if hasattr(other, "lastname"):
            if self.lastname == other.lastname:
                return self.firstname < other.firstname
            return self.lastname < other.lastname
        return NotImplemented

    @classmethod
    def all(cls):
        return db.session.query(cls).all()

    @classmethod
    def get(cls, lastname=None, firstname=None, id=None):
        if lastname and firstname:
            return (
                db.session.query(cls)
                .filter(cls.lastname == lastname)
                .filter(cls.firstname == firstname)
                .one_or_none()
            )
        elif id:
            return db.session.query(cls).filter(cls.id == id).one_or_none()

    named = get

    @property
    def display_name(self):
        return display_name(self.lastname, self.firstname, self.middlename)
