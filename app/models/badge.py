"""
badge.py

models a generic BSA badge or award

"""
from app.extensions import db
from app.utils.logging import logger


class Badge(db.Model):
    __tablename__ = "badge"
    name = db.Column(db.String, primary_key=True)
    version = db.Column(db.String)
    requirements = db.relationship("Requirement", back_populates="badge")
    type = db.Column(db.String)

    __mapper_args__ = {
        "polymorphic_identity": "badge",
        "polymorphic_on": "type",
    }

    def __repr__(self):
        return f"<badge {self.name}>"

    def __iter__(self):
        for r in self.requirements:
            yield r

    def __lt__(self, other):
        return self.name < other.name

    @classmethod
    def named(cls, name):
        logger.debug(f"getting rank {name}")
        logger.debug(f"{db.session.query(cls).filter(cls.name == name).one_or_none()}")
        return db.session.query(cls).filter(cls.name == name).one_or_none()

    @classmethod
    def all(cls):
        return db.session.query(cls).all()


class Award(Badge):
    __tablename__ = "award"

    __mapper_args__ = {"polymorphic_identity": "award"}

    def __repr__(self):
        return f"<Award: {self.name}>"
