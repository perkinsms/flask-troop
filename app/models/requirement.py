"""
requirement.py

models a BSA rank requirement

"""
from app.extensions import db
from app.utils.logging import logger


class Requirement(db.Model):
    __tablename__ = "requirement"
    badge_name = db.Column(db.String, db.ForeignKey("badge.name"))
    badge = db.relationship(
        "Badge", back_populates="requirements", foreign_keys=[badge_name]
    )
    code = db.Column(db.String, primary_key=True)
    shortcode = db.Column(db.String)
    text = db.Column(db.String)
    full_text = db.Column(db.String)
    groups = db.relationship(
        "RGroup", secondary="requirement_rgroup", back_populates="requirements"
    )
    signoffs = db.relationship("Signoff", back_populates="requirement")
    type = db.Column(db.String)

    __mapper_args__ = {
        "polymorphic_identity": "requirement",
        "polymorphic_on": "type",
    }

    def __repr__(self):
        return f"<{self.badge_name}-{self.shortcode}: {self.text}>"

    def __lt__(self, other):
        if self.type == other.type:
            return (self.badge, self.shortcode) < (
                other.badge,
                other.shortcode,
            )

    @classmethod
    def get(cls, code):
        logger.debug(f"getting requirement {code}")
        return db.session.query(cls).filter(cls.code == code).one_or_none()

    get_string = get

    @classmethod
    def all(cls):
        return db.session.query(cls).all()


class RankRequirement(Requirement):
    __tablename__ = "rank_requirement"

    __mapper_args__ = {
        "polymorphic_identity": "rank_requirement",
    }

    def need_count(self, scouts_list):
        return len(scouts_list) - len([s for s in scouts_list if s.signoff(self)])


class MBRequirement(Requirement):
    __tablename__ = "mb_requirement"

    __mapper_args__ = {
        "polymorphic_identity": "mb_requirement",
    }


class AwardRequirement(Requirement):
    __tablename__ = "award_requirement"

    __mapper_args__ = {
        "polymorphic_identity": "award_requirement",
    }
