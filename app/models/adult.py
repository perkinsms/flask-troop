"""
adult.py

"""
from app.extensions import db
from .user import User


class Adult(User):
    __tablename__ = "adult"
    id = db.Column(db.Integer, db.ForeignKey("user.id"), primary_key=True)
    children = db.relationship(
        "Scout", secondary="parent_child", back_populates="parents"
    )

    __mapper_args__ = {
        "polymorphic_identity": "adult",
    }

    def __repr__(self):
        return f"<Adult {self.lastname} {self.firstname}>"


class ScoutParent(db.Model):
    __tablename__ = "parent_child"
    scout_id = db.Column(db.Integer, db.ForeignKey("scout.id"), primary_key=True)
    adult_id = db.Column(db.Integer, db.ForeignKey("adult.id"), primary_key=True)
