"""
merit_badge.py

models a BSA merit badge

"""
from app.extensions import db
from .badge import Badge


class MeritBadge(Badge):
    __tablename__ = "meritbadge"
    eagle_required = db.Column(db.Boolean)

    __mapper_args__ = {"polymorphic_identity": "meritbadge"}

    def __repr__(self):
        return f"<Merit Badge: {self.name}>"
