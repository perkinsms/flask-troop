"""
signoff.py

"""
from app.extensions import db


class Signoff(db.Model):
    __tablename__ = "signoff"
    date = db.Column(db.Date)
    scout_id = db.Column(db.Integer, db.ForeignKey("scout.id"), primary_key=True)
    scout = db.relationship("Scout", back_populates="signoffs")
    signer_id = db.Column(db.Integer, db.ForeignKey("adult.id"))
    badge_name = db.Column(db.String, db.ForeignKey("badge.name"))
    badge = db.relationship("Badge")
    code = db.Column(db.String, db.ForeignKey("requirement.code"), primary_key=True)
    shortcode = db.Column(db.String)
    requirement = db.relationship("Requirement", back_populates="signoffs")
    type = db.Column(db.String)

    __mapper_args__ = {
        "polymorphic_identity": "signoff",
        "polymorphic_on": "type",
    }

    def __repr__(self):
        date = self.date.strftime("%m/%d/%Y") if self.date else "N/A"
        return f"<signoff of {self.badge_name}-{self.code} for {self.scout.firstname} {self.scout.lastname} on {date}>"

    def __lt__(self, other):
        if self.type == other.type:
            return (self.badge_name, self.shortcode) < (
                self.badge_name,
                other.shortcode,
            )

    @classmethod
    def get(cls, scout_id=scout_id, code=code):
        return (
            db.session.query(cls)
            .filter(cls.scout_id == scout_id)
            .filter(cls.code == code)
            .one_or_none()
        )


class RankSignoff(Signoff):
    __tablename__ = "rank_signoff"

    __mapper_args__ = {"polymorphic_identity": "rank_signoff"}


class MBSignoff(Signoff):
    __tablename__ = "mb_signoff"

    __mapper_args__ = {"polymorphic_identity": "mb_signoff"}


class AwardSignoff(Signoff):
    __tablename__ = "award_signoff"

    __mapper_args__ = {"polymorphic_identity": "award_signoff"}
