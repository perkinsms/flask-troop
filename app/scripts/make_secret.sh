#!/usr/bin/env sh
#
# usage: ./make_secret TROOPNAME
#
#
#
if [ -f "data/$1/.env" ]
then
    echo -n "SECRET_KEY = " > data/$1/.env
    dd if=/dev/urandom bs=30 count=1 | xxd -p >> data/$1/.env
else
    echo "Directory Doesn't exist, exiting"
fi
