from pathlib import Path
from flask import Flask, flash, request, redirect, url_for
from werkzeug.utils import secure_filename

UPLOAD_FOLDER = "data/162/uploads"

ALLOWED_EXTENSIONS = {"pdf"}

app = Flask(__name__)
app.config["UPLOAD_FOLDER"] = UPLOAD_FOLDER


def allowed_file(filename):
    return "." in filename and filename.rsplit(".", 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route("/", methods=["GET", "POST"])
def upload_file():
    if request.method == "POST":
        if "file" not in request.files:
            flash("No file part")
            return redirect(request.url)
        file = request.files["file"]
        if file.filename == "":
            flash("No selected files")
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(Path(app.config["UPLOAD_FOLDER"]) / filename)
            return redirect(url_for("download_file", name=filename))
    return """
<! doctype html>
<title>Upload new file</title>
<h1>Upload new file</h1>
<form method=post enctype=multipart/form-data>
   <input type=file name=file>
   <input type=submit value=Upload>
</form>
"""
