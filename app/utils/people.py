""" 
methods for helping the use of people including their names etc
"""

import re
from nameparser import HumanName
from app.utils.logging import log_it

pat = re.compile(r'^(.*), (.*?)( "(.*)")?$')

# with lru-cache: 20.58 seconds, 94% cpu
# without lru-cache: 20.08s 94% CPU


@log_it
def parse_name(text):
    """takes apart 'Perkins, Michael "Mike"' into a dict"""
    name = HumanName(text.strip())
    return {
        "lastname": name.last,
        "firstname": name.first,
        "middlename": name.middle,
        "nickname": name.nickname,
        "whole_name": f"{name.last}, {name.first}",
    }


def display_name(lastname, firstname, middlename, *, nickname=None, use_nickname=False):
    """given some text, return "Firstname Lastname" or "Nickname Lastname" depending
    on value of use_nickname"""
    if use_nickname and nickname:
        return f"{nickname} {lastname[0]}"
    return f"{firstname} {lastname[0]}"
