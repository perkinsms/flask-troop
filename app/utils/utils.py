from datetime import datetime
import re


def parse_merit_badge(text):
    pat = re.compile(
        r"""^(.*?)          # merit badge name
                          (\ \((.*)\))?  # optional space and version name
                          (\*)?$         # optional * to denote eagle required
                      """,
        re.X,
    )
    m = pat.match(text)
    if m:
        return {
            "name": m.group(1),
            "version": m.group(3) or None,
            "eagle_required": bool(m.group(4)),
        }
    else:
        return {
            "name": text,
            "version": None,
            "eagle_required": False,
        }


def parse_code(text):
    if not isinstance(text, str):
        text = str(text)
    # one or two digits, then a lowercase letter (optional), then a period
    pat = re.compile(r"(\d{1,2})(\.?)([a-z]?)\.?")
    m = pat.match(text)
    if m:
        if len(m.group(1)) == 1:
            # single digit number
            return f"0{m.group(1)}{m.group(3)}"
        else:
            return m.group(1) + m.group(3)
    else:
        return text


def get_date(text):
    try:
        if not text or "_" in text:
            return None
        elif "/" in text:
            return datetime.strptime(text, "%m/%d/%y").date() if text else None
        elif "-" in text:
            return datetime.strptime(text, "%Y-%m-%d").date() if text else None
    except ValueError:
        return text
