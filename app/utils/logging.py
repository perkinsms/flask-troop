import logging
from datetime import datetime
from functools import wraps
from app.config import Config

LOG_FORMAT = "%(asctime)s [%(levelname)s]: %(message)s in %(pathname)s:%(lineno)d"
LOG_LEVEL = logging.DEBUG
LOGFILE = Config.LOG_DIR / "app.log"
logging.basicConfig(filename=LOGFILE, filemode="w", level=LOG_LEVEL)
logging.info("Started")
logger = logging.getLogger(__name__)

# requirements logger
R_LOG_FORMAT = "%(message)s"
R_LOG_LEVEL = logging.INFO
R_LOGFILE = Config.LOG_DIR / "signoffs.log"
requirements_logger = logging.getLogger(__name__ + ".requirements")
requirements_logger.setLevel(R_LOG_LEVEL)
requirements_file_handler = logging.FileHandler(R_LOGFILE)
requirements_file_handler.setLevel(R_LOG_LEVEL)
requirements_file_handler.setFormatter(logging.Formatter(R_LOG_FORMAT))
requirements_logger.addHandler(requirements_file_handler)


def log_it(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        logger.info(
            f'in log_it: {f.__name__} {args} {kwargs} {datetime.today().strftime("%H:%M:%S")}'
        )
        result = f(*args, **kwargs)
        logger.info(f"Returning from {f.__name__}: {result}")
        return result

    return wrapper
