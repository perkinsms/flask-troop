from ..extensions import db
from ..models import Scout, Requirement, Signoff, Rank


def signoffs(scouts=None, ranks=None, requirements=None):
    if requirements:
        return (
            scout_signoffs_query(scouts)
            .intersect(requirement_signoffs_query(requirements))
            .all()
        )
    else:
        return scout_signoffs_query(scouts).intersect(rank_signoffs_query(ranks)).all()


def scout_signoffs_query(scouts=None):
    if not scouts:
        scouts = Scout.all()
    elif not hasattr(scouts, "__iter__"):
        scouts = [scouts]
    scout_lastnames = [i.lastname for i in scouts if hasattr(i, "signoffs")]
    scout_firstnames
    return db.session.query(Signoff).join(Scout).filter(Scout.lastname.in_(scout_names))


def rank_signoffs_query(ranks=None):
    if not ranks:
        ranks = Rank.all()
    elif not hasattr(ranks, "__iter__"):
        ranks = [ranks]
    rank_names = [i.name for i in ranks]
    return (
        db.session.query(Signoff)
        .join(Requirement)
        .join(Rank)
        .filter(Rank.name.in_(rank_names))
    )


def requirement_signoffs_query(requirements=None):
    queries = []
    if not requirements:
        requirements = Requirement.all()
    elif not hasattr(requirements, "__iter__"):
        requirements = [requirements]

    for rank in {r.badge for r in requirements}:
        # get just the codes for this rank
        codes = {r.code for r in requirements if r.badge == rank}
        code_query = code_signoffs_query(codes)
        # if it has the rank we want and the code we want,
        # it's the requirement we want
        queries.append(rank.intersect(code_query))
    # return the query that gets all combinations of rank and code
    return queries[0].union_all(*queries)


def code_signoffs_query(codes=None):
    if not codes:
        codes = {i.code for i in Requirement.all()}
    return db.session.query(Signoff).filter(Signoff.code.in_(codes))


def get_many(cls, scout_ids=None, codes=None):
    return (
        db.session.query(
            Scout.patrol,
            Rank.rank_order,
            Signoff.badge_name,
            Signoff.code,
            Scout.id,
            Scout.name,
            Signoff.date,
            Scout.current_rank,
            Signoff.badge,
        )
        .select_from(Signoff)
        .join(Scout)
        .join(Rank)
        .filter(Scout.id.in_(scout_ids))
        .filter(Signoff.code.in_(codes))
        .all()
    )


Signoff.get_many = classmethod(get_many)
