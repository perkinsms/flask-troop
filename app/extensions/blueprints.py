from app.utils.logging import logger
from app.blueprints.main import bp as main_bp
from app.blueprints.scouts import bp as scouts_bp
from app.blueprints.patrols import bp as patrols_bp
from app.blueprints.advancement import bp as advancement_bp
from app.blueprints.announcements import bp as announcements_bp
from app.blueprints.admin import bp as admin_bp
from app.blueprints.signoffs import bp as signoffs_bp
from app.blueprints.merit_badges import bp as merit_badges_bp
from app.blueprints.users import bp as users_bp
from app.blueprints.uploads import bp as uploads_bp
from app.blueprints.groups import bp as groups_bp
from app.blueprints.requirement_groups import bp as requirement_groups_bp


def load_blueprints(app):
    """register all the blueprints"""
    app.register_blueprint(main_bp)
    logger.info("registered the main")

    app.register_blueprint(scouts_bp, url_prefix="/scouts")
    logger.info("registered the scouts")

    app.register_blueprint(patrols_bp, url_prefix="/patrols")
    logger.info("registered the patrols")

    app.register_blueprint(advancement_bp, url_prefix="/advancement")
    logger.info("registered the advancement")

    app.register_blueprint(announcements_bp, url_prefix="/announcements")
    logger.info("registered the announcements")

    app.register_blueprint(admin_bp, url_prefix="/admin")
    logger.info("registered admin")

    app.register_blueprint(signoffs_bp, url_prefix="/signoff")
    logger.info("registered signoff")

    app.register_blueprint(merit_badges_bp, url_prefix="/merit_badges")
    logger.info("registered merit_badges")

    app.register_blueprint(users_bp, url_prefix="/users")
    logger.info("registered users")

    app.register_blueprint(uploads_bp, url_prefix="/uploads")
    logger.info("registered uploads")

    app.register_blueprint(groups_bp, url_prefix="/groups")
    logger.info("registered groups")

    app.register_blueprint(requirement_groups_bp, url_prefix="/requirement_groups")
    logger.info("registered requirement_groups")
