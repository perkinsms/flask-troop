from app.utils.logging import logger
import app.loaders as load
import click
from .database import db


def load_commands(app):
    app.cli.add_command(load_all)
    app.cli.add_command(load_signoffs)
    app.cli.add_command(load_mb_completions)
    app.cli.add_command(load_presets)
    app.cli.add_command(init_db)
    app.cli.add_command(get_tm_data)
    app.cli.add_command(load_variable)


@click.command("load-all")
def load_all():
    logger.info("starting load_all")
    load.all()
    click.echo("loaded the db")


@click.command("load-variable")
def load_variable():
    logger.info("starting load_variable")
    load.variable()
    click.echo("loaded the db")


@click.command("load-signoffs")
def load_signoffs():
    logger.info("starting load_signoffs")
    load.load_signoffs()
    click.echo("loaded signoffs")


@click.command("load-presets")
def load_presets():
    logger.info("starting load_presets")
    load.load_requirement_groups()
    click.echo("loaded presets")


@click.command("load-mb-completions")
def load_mb_completions():
    logger.info("starting load_mb_completions")
    load.load_mb_completions()
    click.echo("loaded mb_completions")


@click.command("init-db")
def init_db():
    logger.info("initializing database")
    db.drop_all()
    db.create_all()


@click.command("get-tm-data")
def get_tm_data():
    logger.info("getting tm data")
    from app.loaders.tm_whole_parser import main

    main()
