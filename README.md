# Flask-Troop

## Description

Flask-Troop is a pure python tool to analyze, filter and view BSA troop advancement information. The tool is intended to allow patrol leaders, mentors, scoutmaster and others to plan troop and patrol activities based on the needs of the scouts in their care. Troopmaster and Troop Web Host offer similar features in PDF format and with less flexibility. 

The project uses sqlite3 for database, Flask for http requests, Flask-sqlalchemy, HTMX for page interaction. 

## Features

### Patrols Page

Lists all patrols, then all scouts in each patrol with recent rank information. Patrol links go to patrol pages that list all scouts and what requirements they still need. Work in progress

### Advancement Page

Has a number of filters at the top of the page. You can filter by patrol and scout, and by rank, requirement and requirement presets. Presets are configured in data/TROOP/requirement_groups.toml

If no patrol is selected, default is all scouts ordered by last name. If one or more patrols is selected, result is output by patrol. 

Scouts that have completed all requirements requested, or ranks requested, are excluded. 

A checkbox controls whether the requirements are sorted within ranks by the number of scouts that need to complete it, descending.

## Installation

prerequisites: python and pip (install pip with 'python -m ensurepip --upgrade')

git clone https://gitlab.com/perkinsms/flask-troop && cd flask-troop

python -m venv venv

linux: source venv/bin/activate

windows: venv/Scripts/activate.bat

pip install --upgrade pip

pip install -r requirements.txt

linux: echo "TROOP = 'example_troop'" > .env

windows: echo TROOP='example_troop' > .env

linux: ./make_secret.sh example_troop > .env

(if you're just testing, you're ready - skip to "Building the current database")

### Importing your troop data (from Troopwebhost) (optional - there is an example troop included in the repo)

In Troop Web Host, obtain these files:

| Location | Save As | 
| ---- | ---- |
|members/troop directories/scout directory/open in excel |data/{TROOP_NAME}/{TROOP_NAME}_scouts.csv|
|members/troop directories/adult directory/open in excel|data/{TROOP_NAME}/{TROOP_NAME}_adults.csv|
|advancement/maintain advancement/export rank requirements status to excel|data/{TROOP_NAME}/{TROOP_NAME}_rank_signoffs.csv|
|advancement/advancement status reports/merit badge history by scout by date earned/open in excel|data/{TROOP_NAME}/{TROOP_NAME}_merit_badges.csv|
|advancement/requirement reports/uncompleted merit badge requirements by scout|data/{TROOP_NAME}/{TROOP_NAME}_merit_badge_signoffs.csv|

cp -r data/example_troop data/{your troop name or number}

place all files in data/{troop name or number}/raw/

linux: echo "TROOP = '{your troop name or number}'" > .env

windows: echo TROOP='{your troop name or number}' > .env

### Building the current database

flask init-db

flask load-all

(this step may take a minute or two)

## How to use

flask run

open a browser and point it to http://localhost:5000

## Collaborators: @perrydc (advice). 

## License: MIT License

## Copyright: (C) 2023 Michael Perkins
