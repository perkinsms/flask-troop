# Overall structure


## Models
- app/models - files that define the working data model
  - users - defines user objects, which at the present time can be either
    - scout - a scout object that can be in a patrol and have advancement information
    - adult - an adult object
  - badge - defines a badge object, that is a collection of requirements
    - merit badge - a merit badge object
    - rank - a rank badge object
  - signoff - defines an object that records when a scout has completed a certain badge requirement
  - role - defines a user-based role
  - group - defines a grouping of user objects (patrols or other groups)
  - rgroup - defines a grouping of rank requirements (used for filtering)

