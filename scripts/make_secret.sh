#!/usr/bin/env sh
#
# usage: ./make_secret TROOPNAME
#
#
#
#

data_dir="data"
if [ -f $data_dir"/$1/.env" ]
then
    echo -n "SECRET_KEY = " >> $data_dir"/$1/.env"
    dd if=/dev/urandom bs=30 count=1 | xxd -p >> $data_dir"/$1/.env"
else
    echo "Directory Doesn't exist, exiting"
fi
